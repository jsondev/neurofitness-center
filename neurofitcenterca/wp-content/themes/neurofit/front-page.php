<?php get_header(); ?>


<!-- Flex full width slider -->
<div class="flexslider">
    <ul class="slides">
        <?php if( have_rows('slider') ):
 // loop through the rows of data
        while ( have_rows('slider') ) : the_row();  ?>
        <li class="slide slide--phone" data-text="<?php the_sub_field('slider_caption'); ?>">
            <a href="<?php the_sub_field('slider_url'); ?>" > <img src="<?php the_sub_field('slider_image'); ?>" alt="<?php the_sub_field('slider_caption'); ?>" /></a>
        </li>
        <?php
        endwhile;
        else :
                    // no rows found
            endif; ?>
    </ul>
</div>

<!-- end revolution full width slider -->
<div class="scheduleAppointment">
    <div class="row">
        <div class="col-xs-12 col-md-3 col-md-offset-5 col-lg-6 col-sm-3 col-sm-offset-5 col-lg-offset-3 top-schedule-button">
            <a href="<?php the_field('schedule_appointment_link'); ?>"><?php the_field('schedule_appointment_button_text'); ?></a>
        </div>
    </div>
</div>
<section class="container">
    <h2 class="block-title--top-larger neuro-block-title-header"><?php the_field('first_motto_header'); ?></h2>
    <h3 class="block-title neuro-block-title-subheader"><?php the_field('first_motto_subheader'); ?></h3>
</section><!-- end container -->

<!-- Slider with 2 sides arrow -->
<div class="full-carousel carousel-present-sm">
    <div class="container">
        <div class="swiper-container carousel-sides">
            <div class="swiper-wrapper custom_slider_styling">
                <!--Slide-->
                <?php $the_query=new WP_Query(array( 'cat'=> 4 , 'orderby' => 'rand') ); ?>
                <?php if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 
                        <a href="<?php the_permalink(); ?>" class="swiper-slide" data-src="<?php the_field('slider_service_icon') ?>" data-head="<?php the_title(); ?>">
                            <h2><?php the_field('slider_title'); ?></h2>
                            <h3><?php the_field('slider_service_name'); ?></h3>
                            <p><?php the_field('slider_service_description'); ?></p>
                                                  <span class="image-container image-container--border position-bottom">
                            <img src="<?php the_field('slider_service_icon') ?>" class="img-responsive align-center" />
                            </span>
                        </a>

                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                <?php else : ?>
                   <h1>No Post Found</h1>
                   <?php get_search_form(); ?>
               <?php endif; ?>
            </div>
        </a>
    </div>
    <!--end swiper wrapper-->
</div>
<!--end swiper container-->
<div class="leftside-arrow"> <i class="fa fa-angle-left"></i>
    <div class="slide-preview">
        <img class="img-arrow img-prev" src="#" alt=""> <span class="arrow-heading"></span>
    </div>
</div>
<div class="rightside-arrow"> <i class="fa fa-angle-right"></i>
    <div class="slide-preview">
        <img class="img-arrow img-next" src="#" alt=""> <span class="arrow-heading"></span>
    </div>
</div>
<!--end swiper controls-->
</div>
<!-- end slider with 2 sides arrow -->
</div>
<!--line divider container-->
<div class="container">
    <div class="row">
        <div class="col-lg-12 block-title"></div>
    </div>
</div>
<!-- end line divider container -->
<!--Opening of 2nd motto-->
<section class="container">
    <div class="row motto-2">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
            <h3><span class="forbody"><?php the_field('second_motto_left'); ?></span></h3>
            <p><?php the_field('second_motto_body_content_left'); ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
            <h3><span class="formind"><?php the_field('second_motto_right_header'); ?></span></h3>
            <p><?php the_field('second_motto_body_content_right'); ?></p>
        </div>
    </div>
</section>
<!---end of 2nd motto section-->

<!--line divider container-->
<div class="container">
    <div class="row">
        <div class="col-lg-12 block-title"></div>
    </div>
</div>
<!-- end line divider container -->


<div class="flexslider container">
    <ul class="slides">
                <!--Slide-->
                <?php if( have_rows('video_slider') ):
 // loop through the rows of data
        while ( have_rows('video_slider') ) : the_row();  ?>
                        
                            <li class="slide slide--phone embed-responsive embed-responsive-16by9 wellness-vid">
            <?php the_sub_field('wellness_video'); ?>
        </li>

                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                <?php else : ?>

               <?php endif; ?>
          </ul>
</div>
</div>
<?php get_footer(); ?>