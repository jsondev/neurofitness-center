<?php 
/**
 * Template Name: Template C
 */
?>
<?php get_header(); ?>

<!-- Flex full width slider -->
<div class="flexslider">
    <ul class="slides">
        <?php if( have_rows('slider') ):
 // loop through the rows of data
        while ( have_rows('slider') ) : the_row();  ?>
        <li class="slide slide--phone" data-text="<?php the_sub_field('slider_caption'); ?>">
            <img src="<?php the_sub_field('slider_image'); ?>" alt="<?php the_sub_field('slider_caption'); ?>" />
        </li>
        <?php
        endwhile;
        else :
                    // no rows found
            endif; ?>
    </ul>
</div>
<section class="container-fluid programs neuro_optimal_page">
    <div class="row programs_top_area">
        <div class="col-sm-10 col-sm-offset-1 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-2 top_text_area">
            <?php the_field('top_body_content'); ?>
        </div>
        <div class="col-md-6 col-lg-6">
            <img class="top_right_image img-responsive" src="<?php the_field('content_top_right_image'); ?>" alt="" />
        </div>
    </div>
    <section class="scheduleAppointment">
        <div class="row">
            <div class="col-xs-12 col-md-3 col-md-offset-5 col-lg-6 col-sm-3 col-sm-offset-5 col-lg-offset-3 top-schedule-button">
                <a href="<?php the_field('schedule_appointment_link'); ?>"><?php the_field('schedule_appointment_button_text'); ?></a>
            </div>
        </div>
    </section>
    <div class="row middle_content_container">
        <div class="col-lg-6">
            <div class="row middle_content_left_top_text_container">
                <div class="col-lg-7 col-lg-offset-2 col-md-8 col-md-offset-2">
                    <?php the_field('middle_left_top_content'); ?>
                </div>
            </div>
            <div class="row middle_content_left_bottom_text_container">
                <div class="col-lg-7 col-lg-offset-2 col-md-8 col-md-offset-2">
                    <?php the_field('middle_content_left_bottom_text'); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row middle_content_right_text_container" >
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                    <?php the_field('middle_content_right_text'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row programs_bottom_area">
        <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
            <p><?php the_field('bottom_section_full_text_area'); ?></p>
        </div>
    </div>
</section>

<?php get_footer(); ?>


