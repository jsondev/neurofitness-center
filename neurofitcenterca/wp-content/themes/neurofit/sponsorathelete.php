<?php 
/**
 * Template Name: Sponsored Atheletes Page
 */
?>
<?php get_header(); ?>
<div class="container sponsored-athlete-page programs">
	<div class="row">
		<div class="col-lg-12 title">
			<h1><?php the_title(); ?></h1>
			<!--line divider-->
			<div class="col-lg-12 block-title"></div>
		</div>
		<div class="col-lg-12">
		<p><?php the_field('page_summary'); ?></p>
		</div>
		<!--line divider-->
		<div class="col-lg-12 block-title"></div>
	</div>
	<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<?php if( have_rows('sponsored_athlete') ):
 // loop through the rows of data
			while ( have_rows('sponsored_athlete') ) : the_row();  ?>
			
				<div class="col-lg-4 col-md-3 col-sm-12 col-xs-12 athlete-profile">
					<div class="row">
						<div class="col-lg-12">
							<img class="img-responsive" src="<?php the_sub_field('athlete_profile_picture'); ?>" />
						</div>
						<div class="col-lg-12 ">
						<h2 class="text-center"><?php the_sub_field('athlete_full_name'); ?></h2>
						</div>
						<div class="col-lg-12 athlete-description">
						<?php the_sub_field('athlete_description'); ?>
						</div>
						<a href="<?php the_sub_field('athlete_url'); ?>" role="button" class="btn btn-info pull-right">Discover More</a>
					</div>
				</div>
			
			<?php endwhile; ?>
			<?php else : ?>
			<?php wp_reset_query(); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>
