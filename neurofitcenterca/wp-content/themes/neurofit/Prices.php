<?php 
/*
 Template Name: Prices
 */
    ?>
<?php get_header(); ?>


<!-- Flex full width slider -->
<div class="flexslider">
    <ul class="slides">
        <li class="slide slide--phone" data-text="Great pixel perfect template which suites any needs. Some subheader text goes here.">
            <?php if( get_field('price_slider_image') ): ?>
                <img src="<?php the_field('price_slider_image'); ?>" />
            <?php endif; ?>
        </li>
    </ul>
</div>
<div class="container prices">
<a name="<?php the_field('title_one'); ?>"></a>
    <h1 class="block-title--top-larger neuro-block-title-header"><?php the_field('title_one'); ?></h1>
    <!--line divider-->
    <div class="col-lg-12 block-title"></div>
    <div class="row price-spacing">
        <?php $the_query=new WP_Query(array( 'cat'=> 6) ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 


                <div class="col-lg-4 col-md-4">
                    <div class="price-container">
                        <section><?php the_field('section_title'); ?></section>
                        <h2><?php the_field('price_title'); ?></h2>
                        <p><?php the_field('price_content'); ?></p>
                        <p><?php the_field('price_time_cost'); ?></p>
                        <?php the_field('price_image'); ?>
                    </div>
                </div>
            <?php endwhile; ?>
<?php wp_reset_postdata(); ?>
        <?php else : ?>
            
            <h1>No Post Found</h1>
            <?php get_search_form( ); ?>

        <?php endif; ?>

    </div>
    <a name="<?php the_field('title_two'); ?>"></a>
    <h1 class="block-title--top-larger neuro-block-title-header"><?php the_field('title_two'); ?></h1>
    <!--line divider-->
    <div class="col-lg-12 block-title"></div>
    <div class="row price-spacing">
        <?php $the_query=new WP_Query(array( 'cat'=> 8) ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 


                <div class="col-lg-4 col-md-4">
                    <div class="price-container">
                        <section><?php the_field('section_title'); ?></section>
                        <h2><?php the_field('price_title'); ?></h2>
                        <p><?php the_field('price_content'); ?></p>
                        <p><?php the_field('price_time_cost'); ?></p>
                        <?php the_field('price_image'); ?>
                    </div>
                </div>
            <?php endwhile; ?>
<?php wp_reset_postdata(); ?>
        <?php else : ?>
            
            <h1>No Post Found</h1>
            <?php get_search_form( ); ?>

        <?php endif; ?>

    </div>
    <a name="<?php the_field('title_three'); ?>"></a>
    <h1 class="block-title--top-larger neuro-block-title-header"><?php the_field('title_three'); ?></h1>
    <!--line divider-->
        <div class="col-lg-12 block-title"></div>

    <div class="row month-prices">
            <a href="<?php the_field('price_title_bronze_link'); ?>">
        <div class="col-lg-4 col-md-4">
        <div class="price-container bronze-member">
                <section><?php the_field('price_title_bronze'); ?></section>
                <p><?php the_field('price_content_bronze'); ?></p>
                <section><?php the_field('price_bronze_month'); ?></section>
                </div>
        </div>
            </a>
            <a href="<?php the_field('price_title_silver_link'); ?>">
        <div class="col-lg-4 col-md-4">
        <div class="price-container silver-member">
                <section><?php the_field('price_title_silver'); ?></section>
                <p><?php the_field('price_content_silver'); ?></p>
                <section><?php the_field('price_silver_month'); ?></section>
                </div>
        </div>
            </a>
            <a href="<?php the_field('price_title_gold_link'); ?>">
        <div class="col-lg-4 col-md-4">
        <div class="price-container gold-member">
                <section><?php the_field('price_title_gold'); ?></section>
                <p><?php the_field('price_content_gold'); ?></p>
                <section><?php the_field('price_gold_month'); ?></section>
                </div>
        </div>
            </a>
    </div>
<a name="<?php the_field('title_four'); ?>"></a>
    <h1 class="block-title--top-larger neuro-block-title-header"><?php the_field('title_four'); ?></h1>
    <!--line divider-->
    <div class="col-lg-12 block-title"></div>
    <div class="row price-spacing">
        <?php $the_query=new WP_Query(array( 'cat'=> 7) ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?> 


                <div class="col-lg-4 col-md-4">
                    <div class="price-container">
                        <section><?php the_field('section_title'); ?></section>
                        <h2><?php the_field('price_title'); ?></h2>
                        <p><?php the_field('price_content'); ?></p>
                        <p><?php the_field('price_time_cost'); ?></p>
                        <?php the_field('price_image'); ?>
                    </div>
                </div>
            <?php endwhile; ?>
<?php wp_reset_postdata(); ?>
        <?php else : ?>
            
            <h1>No Post Found</h1>
            <?php get_search_form( ); ?>

        <?php endif; ?>

    </div>



</div>

<?php get_footer(); ?>


