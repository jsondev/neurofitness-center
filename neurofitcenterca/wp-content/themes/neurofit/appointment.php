<?php /* Template Name: Appointment Page */ ?>
    <section class="scheduleAppointment">
        <div class="row">
            <div class="col-xs-12 col-md-3 col-md-offset-5 col-lg-6 col-sm-3 col-sm-offset-5 col-lg-offset-3 top-schedule-button">
                <a href="<?php the_field('schedule_appointment_link'); ?>"><?php the_field('schedule_appointment_button_text'); ?></a>
            </div>
        </div>
    </section>