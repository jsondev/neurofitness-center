<?php 
/**
* Template Name: Template D
*/
?>
<?php get_header(); ?>


<!-- Flex full width slider -->
<div class="flexslider">
    <ul class="slides">
        <?php if( have_rows('slider') ):
 // loop through the rows of data
        while ( have_rows('slider') ) : the_row();  ?>
        <li class="slide slide--phone" data-text="<?php the_sub_field('slider_caption'); ?>">
            <img src="<?php the_sub_field('slider_image'); ?>" alt="<?php the_sub_field('slider_caption'); ?>" />
        </li>
        <?php
        endwhile;
        else :
                    // no rows found
            endif; ?>
    </ul>
</div>
<section class="container-fluid programs neurotracker_page">
    <div class="row programs_top_area">
        <div class="col-sm-10 col-sm-offset-1 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-2 top_text_area">
            <h1><?php the_field('top_content_header'); ?></h1>
                <?php the_field('top_content_area'); ?>
        </div>
        <div class="col-md-6 col-lg-6 top_right_image_container">
        <img class="full_width_image img-responsive" src="<?php the_field('top_right_image'); ?>" alt="" />
        </div>
    </div>
    <div class="row">
        <img class="full_width_image img-responsive" src="<?php the_field('middle_image'); ?>" alt="" />
    </div>
    <div class="row programs_bottom_area">
        <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
            <h1><?php the_field('bottom_content_header'); ?></h1>
                <?php the_field('bottom_content_area'); ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>