<?php /* Template Name: Athlete Page */ ?>
<?php get_header(); ?>
<div class="person-wrap">
    <!-- Section with base info about athlete -->
    <section class="container">
        <div class="person">
            <div class="image-container image-container--max image-container--border-inner person__image">
                <img src="<?php the_field('athlete_picture'); ?>" alt="">
            </div>
            <h2 class="person__name"><?php the_field('athlete_name'); ?></h2>
            <p class="person__position">
                <i><?php the_field( 'athlete_position'); ?></i>
            </p>
            <div class="social social--primary person__social">
                <!-- List with social icons -->
                <ul>
                    <li class="social__item"><a class="social__link" href="<?php the_field('personal_twitter'); ?>" target="_blank"><i class="social__icon fa fa-twitter"></i></a>
                    </li>
                    <li class="social__item"><a class="social__link" href="<?php the_field('personal_facebook'); ?>" target="_blank"><i class="social__icon fa fa-facebook"></i></a>
                    </li>
                    <li class="social__item"><a class="social__link" href="<?php the_field('personal_google_plus'); ?>" target="_blank"><i class="social__icon fa fa-google-plus"></i></a>
                    </li>
                    <li class="social__item"><a class="social__link" href="<?php the_field('personal_pinterest'); ?>" target="_blank"><i class="social__icon fa fa-pinterest"></i></a>
                    </li>
                    <li class="social__item"><a class="social__link" href="<?php the_field('personal_tumblr'); ?>" target="_blank"><i class="social__icon fa fa-tumblr"></i></a>
                    </li>
                    <li class="social__item"><a class="social__link" href="<?php the_field('personal_linkedin'); ?>" target="_blank"><i class="social__icon fa fa-linkedin"></i></a>
                    </li>
                    <li class="social__item"><a class="social__link" href="<?php the_field('personal_youtube'); ?>" target="_blank"><i class="social__icon fa fa-youtube"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
</div>
<section class="container">
    <div class="col-sm-10 col-sm-offset-1">
        <h4><?php the_field('bottom_header'); ?></h4>
        <p><?php the_field('bottom_content'); ?></p>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="block-title"><?php the_field('what_i_do_header'); ?></h2>
        </div>
        <div class="col-sm-6">
            <div>
               <h3 class="heading-helper heading-helper--thin">Skills</h3>
               <!-- Secondary progress bar with white space -->
               <div class="progress-container progress--secondary progress-cut" id="progress-start">
                <div class="progress-quantity">
                    <?php the_field( 'first_skill'); ?>: <span class="progress-value">0</span>%</div>
                    <div class="progress" data-level="<?php the_field('first_skill_percentage'); ?>">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
                    </div>
                </div>
                <!-- Secondary progress bar with white space -->
                <div class="progress-container progress--secondary progress-cut">
                    <div class="progress-quantity">
                        <?php the_field( 'second_skill'); ?> <span class="progress-value">0</span>%</div>
                        <div class="progress" data-level="<?php the_field('second_skill_percentage'); ?>">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                        </div>
                    </div>
                    <!-- Secondary progress bar with white space -->
                    <div class="progress-container progress--secondary progress-cut">
                        <div class="progress-quantity">
                            <?php the_field( 'third_skill'); ?> <span class="progress-value">0</span>%</div>
                            <div class="progress" data-level="<?php the_field('third_skill_percentage'); ?>">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                            </div>
                        </div>
                        <!-- Secondary progress bar with white space -->
                        <div class="progress-container progress--secondary progress-cut">
                            <div class="progress-quantity">
                                <?php the_field( 'fourth_skill'); ?> <span class="progress-value">0</span>%</div>
                                <div class="progress" data-level="<?php the_field('fourth_skill_percentage'); ?>">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                </div>
                            </div>
                            <!-- Secondary progress bar with white space -->
                            <div class="progress-container progress--secondary progress-cut">
                                <div class="progress-quantity">
                                    <?php the_field( 'fifth_skill'); ?> <span class="progress-value">0</span>%</div>
                                    <div class="progress" data-level="<?php the_field('fifth_skill_percentage'); ?>">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->
                        <div class="col-sm-6 opposite-block">
                           <h3 class="heading-helper heading-helper--thin"><?php the_field('statistics_header'); ?></h3>
                           <div id="number-start">
                            <!-- Default stat view -->
                            <div class="stat-row">
                                <div class="stat stat--additional"> <span class="stat__number" data-result="<?php the_field('experience_value'); ?>" data-value="<?php the_field('experience_value'); ?>">0</span>
                                   <i class="stat__icon fa fa-calendar-o"></i>
                                   <p class="stat__dimension"><?php the_field('experience_label'); ?></p>
                               </div>
                               <div class="stat stat--additional"> <span class="stat__number" data-result="<?php the_field('awards_value'); ?>" data-value="<?php the_field('awards_value'); ?>">0</span>
                                   <i class="stat__icon fa fa-trophy"></i>
                                   <p class="stat__dimension"><?php the_field('awards_label'); ?></p>
                               </div>
                               <div class="stat stat--additional"> <span class="stat__number" data-result="<?php the_field('project_value'); ?>" data-value="<?php the_field('project_value'); ?>">0</span>
                                   <i class="stat__icon fa fa-folder"></i>
                                   <p class="stat__dimension"><?php the_field('project_label'); ?></p>
                               </div>

                               <div class="stat stat--additional"> <span class="stat__number" data-result="<?php the_field('book_value'); ?>" data-value="<?php the_field('book_value'); ?>">0</span>
                                <i class="stat__icon fa fa-book"></i>
                                <p class="stat__dimension"><?php the_field('book_label'); ?></p>
                            </div>

                            <div class="stat stat--additional">  <span class="stat__number" data-result="<?php the_field('post_value'); ?>" data-value="<?php the_field('post_value'); ?>">0</span>
                                <i class="stat__icon fa fa-file-text-o"></i>
                                <p class="stat__dimension"><?php the_field('post_label'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </section>
    <div class="scheduleAppointment">
        <div class="row">
            <div class="col-xs-12 col-md-3 col-md-offset-5 col-lg-6 col-sm-3 col-sm-offset-5 col-lg-offset-3 top-schedule-button">
                <a href="<?php the_field('schedule_appointment_link'); ?>"><?php the_field('schedule_appointment_text'); ?></a>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>