<!DOCTYPE html>
<html lang="en-us">
<head>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <?php wp_head(); ?>
    <script type="text/javascript">
jQuery(document).ready(function() {
    jQuery("#textsizer a").textresizer({
        target: "body"
    });
});
</script>
</head>

<body>
    <div class="wrapper" id="top">
        <!-- Header section -->
        <header class="header header--thin header--light">
            <div class="header-fixed">
            <div class="header-line waypoint" data-animate-down="header-up" data-animate-up="header-down">
            
            <div class="fixed-top header-down">
            <div class="container">
                <!--  Logo  -->
                    <a class="logo" href="<?php bloginfo('url'); ?>">
                        <!-- Remove comments to choose image and add comment to h1 -->
                        <img src="http://neurofitcenter.ca/neurofitinstall/wp-content/uploads/2015/05/logo_03.png" alt="">

                        <!--<h1 class="logo__text">Neurofitness Center</h1>    -->         
                    </a>
                    <!-- End Logo -->
                <!-- End Logo -->

                <!-- Navigation section -->
                <nav class="z-nav">
                    <!-- Toggle for menu mobile view -->
                    <a href="#" class="z-nav__toggle">
                        <span class="menu-icon"></span>
                        <span class="menu-text">navigation</span>
                        <div class="menu-head"></div>
                    </a>
                    <div class="z-nav-inner">
<!--<script>$('.z-nav__link').prepend('<span class="z-nav__toggle-sub plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>')</script>-->

                    <?php 
                    wp_nav_menu( array(
                        'theme_location'    => 'front_page_menu',
                        'container'     => false,
                        'container_class' => '',
                        'menu_class'        => 'z-nav__list', 
                        'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'walker'        => new custom_theme_walker_nav_menu
                        ) ); 
                        ?>
                    <!-- end list menu item -->
                </nav>
                <!-- end navigation section -->
                </div>
                <div class="row">
                <div class="col-md-3 pull-right">
                <div id="textsizer">
   <p>Font Size:</p>
   <ul class="textresizer">
       <li><a href="#nogo">S</a></li>
       <li><a href="#nogo">M</a></li>
       <li><a href="#nogo">L</a></li>
       <li><a href="#nogo">XL</a></li>
   </ul>
</div>
</div>
            </div> <!-- end container -->
            </div> <!-- end fixed top block -->
            </div>
        </header>
        <!-- end header section -->
        