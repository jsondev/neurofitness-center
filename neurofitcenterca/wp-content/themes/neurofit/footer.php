<div class="testimonial-block testimonial-block--home">
    <div class="container">
        <div class="block-title block-title--top-larger">Testimonials</div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="sub-header sub-header--simple">People who believe in us:</h3>
                <div class="content">
                    <ul class="testimonial-wrap">
                        <?php $query = new WP_Query(array('cat' => 3 ,'posts_per_page' => 5, 'orderby' => 'menu_order','order' => 'DESC') ); ?>
                        <?php if($query->have_posts()) : ?>
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <li class="testimonial">
                                    <div class="testimonial__images">
                                        <?php if( get_field('testimonial_quoter_image') ): ?>
                                            <img src="<?php the_field('testimonial_quoter_image'); ?>" />
                                        <?php endif; ?>
                                    </div>
                                    <p class="testimonial__author"><?php the_field('testimonial_quoter_name'); ?></p>
                                    <p class="testimonial__info"><?php the_field('testimonial_quote_pulled_from'); ?></p>
                                    <p class="testimonial__text"><?php the_field('testimonial_quote'); ?></p>
                                </li>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php wp_reset_query(); ?>
                            <h1>No Post Found</h1>
                            <?php get_search_form( ); ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->

    <!-- Footer section -->
    <footer class="footer mobile-footer">
        <div class="container">
            <div class="row">
                <!-- Programs -->
                <div class="col-sm-3">
                    <h3 class="heading-info">PROGRAMS</h3>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 one-column">
                            <ul>
                                <?php if (have_rows('fgp_link_repeater', 'option')) { ?>
                                <?php while ( have_rows('fgp_link_repeater', 'option') ) : the_row(); ?>
                                    <?php if( get_sub_field('fgp_hyperlink_field' ,'option')) : ?> 
                                        <li>
                                            <a href="<?php the_sub_field('fgp_hyperlink_field','option'); ?>">
                                                <?php if( get_sub_field('fgp_link_text' ,'option')) :  the_sub_field('fgp_link_text','option'); else: ''; endif; ?>
                                            </a>
                                        </li> 
                                    <?php else: ''; endif; ?>
                                    <?php endwhile; ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- end programs -->

                    <!-- Center -->
                    <div class="col-sm-3">
                        <h3 class="heading-info">OUR CENTER</h3>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 one-column">
                                <ul>
                                    <?php if (have_rows('fgc_link_repeater', 'option')) { ?>
                                    <?php while ( have_rows('fgc_link_repeater', 'option') ) : the_row(); ?>
                                        <?php if( get_sub_field('fgc_hyperlink_field' ,'option')) : ?> 
                                            <li>
                                                <a href="<?php the_sub_field('fgc_hyperlink_field','option'); ?>">
                                                    <?php if( get_sub_field('fgc_link_text' ,'option')) :  the_sub_field('fgc_link_text','option'); else: ''; endif; ?>
                                                </a>
                                            </li> 
                                        <?php else: ''; endif; ?>
                                        <?php endwhile; ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- end of Center -->

                        <!-- Connect -->
                        <div class="col-sm-3">
                            <h3 class="heading-info">CONNECT</h3>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 one-column">
                                    <ul>
                                        <?php if (have_rows('fgcnt_link_repeater', 'option')) { ?>
                                        <?php while ( have_rows('fgcnt_link_repeater', 'option') ) : the_row(); ?>
                                            <?php if( get_sub_field('fgcnt_hyperlink_field' ,'option')) : ?> 
                                                <li>
                                                    <a href="<?php the_sub_field('fgcnt_hyperlink_field','option'); ?>">
                                                        <?php if( get_sub_field('fgcnt_link_text' ,'option')) :  the_sub_field('fgcnt_link_text','option'); else: ''; endif; ?>
                                                    </a>
                                                </li> 
                                            <?php else: ''; endif; ?>
                                            <?php endwhile; ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- end of Connect -->

                            <!-- Contact Info -->
                            <div class="col-sm-3">
                               <div class="row">
                                <div class="col-xs-12">
                                 <img class="main visible-desktop img-responsive center-block footer-logo" src="http://neurofitcenter.ca/neurofitinstall/wp-content/uploads/2015/03/bottom_logo_03.png" alt="Responsive image" />
                             </div>
                         </div>
                         <div class="row">
                            <div class="col-lg-12 center-block text-center">
                                <ul>
                                    <?php if (have_rows('fgcnt_contact_repeater', 'option')) { ?>
                                    <?php while ( have_rows('fgcnt_contact_repeater', 'option') ) : the_row(); ?>
                                        <?php if( get_sub_field('fgcnt_contact_label' ,'option')) : ?> 
                                            <li>
                                                <a href="<?php the_sub_field('fgc_hyperlink_field','option'); ?>">
                                                    <?php if( get_sub_field('fgcnt_contact_label' ,'option')) :  the_sub_field('fgcnt_contact_label','option'); else: ''; endif; ?>
                                                </a>
                                            </li> 
                                        <?php else: ''; endif; ?>
                                        <?php endwhile; ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- end contact info -->
                    </div><!-- end row -->

                </div><!-- end container -->
            </footer>
            <!-- end footer section -->
        </div>
        <div class="top-scroll-width">
            <div class="top-scroll">
                <div>
                    <i class="fa fa-angle-up"></i>
                </div>
            </div>
            <?php wp_footer() ?>

            <script>
                $(document).ready(function() {
                    flexSlider();
                    scrollSlider();
                    preloader();
                    <?php if ( is_front_page() ) { ?>
                       sliderSides();
                       <?php } ?>

                       <?php if (is_page_template('employee.php') || is_page_template('athlete.php' )){ ?>
                        progressStart();
                        numberStart();
                        <?php  } ?>

                        $('.animated-services').one('inview', function (event, visible) {
                            if (visible == true) {
                                $('.service.start-1').addClass('zoomIn');
                                $('.service.start-2').addClass('zoomIn stage2');
                                $('.service.start-3').addClass('zoomIn stage3');
                                $('.service.start-4').addClass('zoomIn stage4');
                            }
                        });
                        $('.price-start').one('inview', function (event, visible) {
                            if (visible == true) {
                                $('.price-container').addClass('fadeInUp');
                            }
                        });
                        $('.share').one('inview', function (event, visible) {
                            if (visible == true) {
                                $('.share').addClass('fadeInUp');
                            }
                        });
                    });
                </script>
            </body>
            </html>