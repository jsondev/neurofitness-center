<?php
    /**
     * The functions file is used to initialize everything in the theme.  It controls how the theme is loaded and
     * sets up the supported features, default actions, and default filters.  If making customizations, users
     * should create a child theme and make changes to its functions.php file (not this one).  Friends don't let
     * friends modify parent theme files. ;)
     *
     * Child themes should do their setup on the 'after_setup_theme' hook with a priority of 11 if they want to
     * override parent theme features.  Use a priority of 9 if wanting to run before the parent theme.
     *
     * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
     * General Public License as published by the Free Software Foundation; either version 2 of the License,
     * or (at your option) any later version.
     *
     * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
     * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
     *
     * You should have received a copy of the GNU General Public License along with this program; if not, write
     * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
     *
     * @package neurofit
     * @subpackage Functions
     * @version 1.0
     * @author James Anderson <james.anderson@innovativeimaginations.com>
     * @copyright Copyright (c) 2014 - 2015, James Anderson
     * @link http://neurofitcenter.ca/themes/neurofitnessca
     * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
     */

    /* Load the core theme framework. */
    require_once( trailingslashit( get_template_directory() ) . 'library/hybrid.php' );
    new Hybrid();

    /* Do theme setup on the 'after_setup_theme' hook. */
      add_action( 'after_setup_theme', 'neurofit_setup', 10);

    if (!function_exists('neurofit_setup')){

        /**
         * neurofit Theme setup.
         *
         * Set up theme defaults and registers support for various WordPress features.
         *
         * Note that this function is hooked into the after_setup_theme hook, which
         * runs before the init hook. The init hook is too late for some features, such
         * as indicating support post thumbnails.
         *
         * @since Innovative Imaginations Theme v1.0
         */

        function neurofit_setup(){
            /* Add theme support for framework extensions. */
            
            /*  This is to activate the featured image in Posts
             *  The best thumbnail/image script ever. 
             */
            add_theme_support( 'get-the-image' );

            /* Enable custom template hierarchy. */
            add_theme_support( 'hybrid-core-template-hierarchy' );

            /* Load shortcodes. */
            add_theme_support( 'hybrid-core-shortcodes' );

            /*  Adds input fields in the post editor for adding post-specific 
             *  meta information as well as sets up some defaults on other pages.
             */
            add_theme_support( 'hybrid-core-seo' );

            /*  This theme uses wp_nav_menu() in one location. */
            register_nav_menus( array(
                'front_page_menu' => __( 'Front Page Menu', 'neurofit'),
                'default_page_menu' => __( 'Default Page Menu', 'neurofit'),
            ));

            /*  Here are Hybrid Core Framework Extensions which provide more 
             *  Theme Supports
             */
            
            /* Breadcrumbs. Yay! */
            add_theme_support( 'breadcrumb-trail' );
            
            remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
            remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
            remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
            remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
            remove_action( 'wp_head', 'index_rel_link' ); // index link
            remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
            remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
            remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
            remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP ver
            
            // Hook into the header to add the needed meta 
            add_action('wp_head', 'add_meta_tags', 0);

            // Hook into the 'wp_enqueue_scripts' action
            add_action( 'wp_enqueue_scripts', 'neurofit_theme_scripts' );
        
            // Enqueue Scripts in the Admin
            //add_action( 'admin_enqueue_scripts', 'Text Domain_post_listing_column_resize' );       
            
            //Set Image Sizes
            //add_action( 'init', 'text_domain_add_image_sizes' );
            
            /* Filters hooks go here. */

            //enable automatic updates for all plugins
            add_filter( 'auto_update_plugin', '__return_true' );

            // enable automatic updates for all themes
            add_filter( 'auto_update_theme', '__return_true' );
            
            /** Gravity Forms Filters 
             *  These functions are located in includes/forms.php  
            **/
            // Add custom classes to inputs
            //add_action("gform_field_input", "gf_custom_class", 10, 5);
 
            /** Gravity Forms Filters 
              * These functions are located in includes/forms.php    
            **/

            // This function forces jQuery calls to be loaded in the footer after all other scripts
            add_filter("gform_init_scripts_footer", "init_scripts");

            // filter the Gravity Forms button type 
            //add_filter("gform_submit_button", "form_submit_button", 10, 2);

            // Add a class to menu icon
            add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
            
            //Add Prev & Next Link Styled Paginaiton
            add_filter('next_posts_link_attributes', 'next_page_posts_link_attributes');
            add_filter('previous_posts_link_attributes', 'previous_page_posts_link_attributes');

            /**
             * Display column based on Role
             *
             * The column name can be found by opening the settings screen of your column
             * and hover your mouse over "Type". In the tooltip you will find the column Name.
             *
             * In this example we will disable the Custom Field column for the 'subscriber' role
             *
             */
            //add_filter( "manage_edit-post_columns", "myplugin_filter_hide_column_based_on_role", 101, 1 );
            //add_filter( "manage_edit-page_columns", "myplugin_filter_hide_column_based_on_role", 101, 1 );

            
            /**
            * Create Advance Custom Fields Options Pages.
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/options-pages.php');

            /**
            * Register our sidebars and widgetized areas.
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/dynamic-sidebars.php');

            /**
            * Create Breadcrumbs
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/breadcrumbs.php');

            /**
            * Gravity Forms Configuration
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/forms.php');

            /**
            * Create Pagination
            *
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/pagination.php');

            /**
            * Customize Nav Menu
            */
            require_once( trailingslashit( THEME_DIR ) .'includes/walker-menu.php');
        }
    }
    
    //adds meta tags along with the title tag to the header.php file
    if (!function_exists('add_meta_tags')) {
        function add_meta_tags() {
        ?>
            
        <?php
        }
    }

    if (!function_exists('the_slug')) {
        // Get the slug
        function the_slug() {
            $post_data = get_post($post->ID, ARRAY_A);
            $slug = $post_data['post_name'];
            return $slug; 
        }
    }
    
    if (!function_exists('special_nav_class')) {
        function special_nav_class($classes, $item){
             if( in_array('main-menu-item', $classes) ){
                     $classes[] = 'z-nav__item';
             }
             return $classes;
        }  
    }

    //Set Image Sizes
    if (!function_exists('neurofit_add_image_sizes')) {
        function neurofit_add_image_sizes() {
           // add_image_size('featured-image-name', width, height, true/false);
        }
    }

    // Register Script
    if(!function_exists('neurofit_theme_scripts')){
        function neurofit_theme_scripts(){
            global $wp_scripts;
            global $wp_styles;
            
//  Styles
            wp_deregister_style( 'jquerycss' );
            wp_register_style( 'jquerycss','http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/smoothness/jquery-ui.css', false, '1.0');
            wp_enqueue_style( 'jquerycss' );

            wp_deregister_style( 'bootstrap' );
            wp_register_style( 'bootstrap', trailingslashit(THEME_URI).'css/bootstrap.min.css', false, '1.0');
            wp_enqueue_style( 'bootstrap' );

            wp_deregister_style( 'raleway' );
            wp_register_style( 'raleway','http://fonts.googleapis.com/css?family=Raleway', false, '1.0');
            wp_enqueue_style( 'raleway' );

            wp_deregister_style( 'opensans' );
            wp_register_style( 'opensans','http://fonts.googleapis.com/css?family=Open+Sans', false, '1.0');
            wp_enqueue_style( 'opensans' );

            wp_deregister_style( 'exo' );
            wp_register_style( 'exo','http://fonts.googleapis.com/css?family=Exo:400,700', false, '1.0');
            wp_enqueue_style( 'exo' );

            wp_deregister_style( 'fontawesome' );
            wp_register_style( 'fontawesome',trailingslashit( THEME_URI ) .'fonts/font-awesome.min.css', false, '1.0');
            wp_enqueue_style( 'fontawesome' );

            wp_deregister_style( 'z-nav' );
            wp_register_style( 'z-nav',trailingslashit( THEME_URI ) .'external/z-nav/z-nav.css', false, '1.0' );
            wp_enqueue_style( 'z-nav' );

            wp_deregister_style( 'flexslider' );
            wp_register_style( 'flexslider',trailingslashit( THEME_URI ).'external/flexslider/flexslider.css', false, '1.0');
            wp_enqueue_style( 'flexslider' );
    
            wp_deregister_style( 'idangerous' );
            wp_register_style( 'idangerous', trailingslashit( THEME_URI ) .'external/swiper/idangerous.swiper.css', false, '1.0' );
            wp_enqueue_style( 'idangerous' );

            wp_deregister_style( 'touch' );
            wp_register_style( 'touch', trailingslashit( THEME_URI ).'css/touch.css?v=1', false, '1.0');
            wp_enqueue_style( 'touch' );
    

            wp_deregister_style( 'mcustomscroll' );
            wp_register_style( 'mcustomscroll', trailingslashit( THEME_URI ).'external/mCustomScrollbar/jquery.mCustomScrollbar.css', false, '1.0');
            wp_enqueue_style( 'mcustomscroll' );
    
            wp_deregister_style( 'style' );
            wp_register_style( 'style',trailingslashit( THEME_URI ) .'css/style.css?v=1', false, '1.0' );
            wp_enqueue_style( 'style' );

            wp_deregister_style( 'default' );
            wp_register_style( 'default', trailingslashit(THEME_URI). 'style.css', false, '1.0');
            wp_enqueue_style( 'default' );

            wp_deregister_script( 'modernizr' );
            wp_register_script( 'modernizr', trailingslashit( THEME_URI ) .'external/modernizr/modernizr.custom.js', false, '1.0');
            wp_enqueue_script( 'modernizr' );

            wp_deregister_script( 'html5shiv' );
            wp_register_script( 'html5shiv', trailingslashit( THEME_URI ) .'js/html5shiv.js', false, '1.0', true);
            wp_enqueue_script( 'html5shiv' );
                
                
             //Use the wp scripts constant if necesarry
             //global ;
             //Conditonally add scripts if necessary
            $wp_scripts->add_data( 'html5shiv', 'conditional', 'lt IE 9' );

            wp_deregister_script( 'respond' );
            wp_register_script( 'respond','http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js', false, '1.0');
            wp_enqueue_script( 'respond' );
              //Use the wp scripts constant if necesarry
              //global ;
              //Conditonally add scripts if necessary
            $wp_scripts->add_data( 'respond', 'conditional', 'lt IE 9' );

            wp_deregister_style( 'ie9' );
            wp_register_style( 'ie9',trailingslashit( THEME_URI ) .'css/ie9.css', false, '1.0' );
            wp_enqueue_style( 'ie9' );
            //Conditonally add styles if necessary
            wp_style_add_data( 'ie9', 'conditional', 'lt IE 9' );

            wp_deregister_style( 'neurofitness' );
            wp_register_style( 'neurofitness',trailingslashit( THEME_URI ).'css/neurofitness.css', false, '1.0');
            wp_enqueue_style( 'neurofitness' );
    


//Footer scripts
            wp_deregister_script( 'ajax' );
            wp_register_script( 'ajax','http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js', false, '1.0', true);
            wp_enqueue_script( 'ajax' );
    // Bootstrap 3
            wp_deregister_script( 'bootstrapmin' );
            wp_register_script( 'bootstrapmin',trailingslashit( THEME_URI ) .'js/bootstrap.min.js', false, '1.0', true);
            wp_enqueue_script( 'bootstrapmin' );

    // Mobile menu 
            wp_deregister_script( 'jquerymobile' );
            wp_register_script( 'jquerymobile',trailingslashit( THEME_URI ) .'external/z-nav/jquery.mobile.menu.js', false, '1.0', true);
            wp_enqueue_script( 'jquerymobile' );
    // jQuery FlexSlider
            wp_deregister_script( 'flexslider' );
            wp_register_script( 'flexslider',trailingslashit( THEME_URI ) .'external/flexslider/jquery.flexslider-min.js', false, '1.0', true);
            wp_enqueue_script( 'flexslider' );
    // Touch slider - Swiper
            wp_deregister_script( 'idangerous' );
            wp_register_script( 'idangerous',trailingslashit( THEME_URI ) .'external/swiper/idangerous.swiper.js', false, '1.0', true);
            wp_enqueue_script( 'idangerous' );
    // Twitter Feed
            // wp_deregister_script( 'twitterfeed' );
            // wp_register_script( 'twitterfeed',trailingslashit( THEME_URI ) .'external/twitterfeed/twitterfeed.js', false, '1.0', true);
            // wp_enqueue_script( 'twitterfeed' );
    // jQuery Scroll
            wp_deregister_script( 'jqueryscroll' );
            wp_register_script( 'jqueryscroll',trailingslashit( THEME_URI ) .'external/scrollto/jquery.scrollTo.min.js', false, '1.0', true);
            wp_enqueue_script( 'jqueryscroll' );

    // Livicons
            wp_deregister_script( 'livicons' );
            wp_register_script( 'livicons',trailingslashit( THEME_URI ) .'external/livicons/livicons-1.3.min.js', false, '1.0', true);
            wp_enqueue_script( 'livicons' );
            wp_deregister_script( 'livicons2' );
            wp_register_script( 'livicons2',trailingslashit( THEME_URI ) .'external/livicons/raphael-min.js', false, '1.0', true);
            wp_enqueue_script( 'livicons2' );

    //mCustom Scrollbar
            wp_deregister_script( 'mcustomscrollbarjs' );
            wp_register_script( 'mcustomscrollbarjs',trailingslashit( THEME_URI ).'external/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js', false, '1.0',true);
            wp_enqueue_script( 'mcustomscrollbarjs' );
                
    //  Event that will trigger when the element is scrolled in to the viewport                  
            wp_deregister_script( 'inview' );
            wp_register_script( 'inview',trailingslashit( THEME_URI ) .'external/inview/jquery.inview.js', false, '1.0', true);
            wp_enqueue_script( 'inview' );

                        wp_deregister_script( 'textresizer' );
            wp_register_script( 'textresizer',trailingslashit( THEME_URI ) .'js/jquery.textresizer.js', false, '1.0',true);
            wp_enqueue_script( 'textresizer' );

            wp_deregister_script( 'textresizermin' );
            wp_register_script( 'textresizermin',trailingslashit( THEME_URI ) .'js/jquery.textresizer.min.js', false, '1.0',true);
            wp_enqueue_script( 'textresizermin' );
    //  Custom
            wp_deregister_script( 'custom' );
            wp_register_script( 'custom',trailingslashit( THEME_URI ) .'js/custom.js', false, '1.0', true);
            wp_enqueue_script( 'custom' );
        }
    }



